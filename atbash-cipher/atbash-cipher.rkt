#lang racket

(require racket/contract)

(provide (contract-out
          [encode (-> string? string?)]
          [decode (-> string? string?)]))

(define alphabet (string->list "abcdefghijklmnopqrstuvwxyz"))
(define rev-alphabet (reverse (string->list "abcdefghijklmnopqrstuvwxyz")))
(define numerics (string->list "0123456789"))
(define cipher-values (for/list ([l alphabet]
                                 [c rev-alphabet])
                        (cons l c)))

(define (prepare-msg-in m)
  (string->list
   (string-downcase
    (regexp-replace* #rx"[^A-Za-z0-9]" m ""))))

(define (prepare-msg-out m)
  (list->string
   (let insert-at-every-n ([msg m]
                           [n 5])
     (cond
       [(empty? msg)  empty]
       [(= n 0)       (cons #\space
                            (insert-at-every-n msg 5))]
       [else          (cons (car msg)
                            (insert-at-every-n (cdr msg) (- n 1)))]))))

(define (encode m)
  (prepare-msg-out
   (map (λ(Letter)
          (if (member Letter numerics)
              Letter
              (cdr (assoc Letter cipher-values))))
        (prepare-msg-in m))))


(define (decode m)
  (list->string
   (map (λ(Letter)
          (if (member Letter numerics)
              Letter
              (cdr (assoc Letter cipher-values))))
        (prepare-msg-in m))))
