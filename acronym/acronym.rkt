#lang racket

(provide acronym)

(define (acronym in-str)
  (for/fold ([res ""])
            ([str (regexp-split #px"[\\s-]+" in-str)])
    (string-append res
                   (select-letters str))))

(define (select-letters in-str)
  (cond
    [(or (string=? (string-downcase in-str) in-str)
      (string=? (string-upcase in-str) in-str))
     (string-upcase (substring in-str 0 1))]
    [else
      (list->string (filter char-upper-case? (string->list in-str)))]))
