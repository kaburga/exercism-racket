#lang racket

(require racket/contract)

(provide rebase)
;;(provide (contract-out
;;          [rebase (-> (non-empty-listof natural?) natural? natural? (listof natural?))]))

(define (rebase list-digits in-base out-base)
  (cond
    [(eq? #t (bad-input? list-digits in-base out-base))
     #f]
    [else
     (to-base-x out-base
                (to-base-10 in-base list-digits))]))

(define (bad-input? list-digits in-base out-base)
  (or (not (andmap (λ(x) (not (or (>= x in-base)
                                  (< x 0))))
                   list-digits))
      (< out-base 2)
      (< in-base 2)))

(define (to-base-10 in-base list-digits)
  (let ([len (length list-digits)])
    (foldl + 0 (map (λ(x)
                      (set! len (sub1 len))
                      (* x (expt in-base len)))
                    list-digits))))

(define (to-base-x out-base n)
  (if (< n out-base)
      (list n)
      (append (to-base-x out-base
                         (floor (/ n out-base)))
              (list (remainder n out-base)))))

